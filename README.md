# Case Studies for 'A Distributed Task Scheduling Framework for Edge Computing and Cyber-Physical Systems'

| Case Study  | Classification                                                     |
| ----------- | ------------------------------------------------------------------ |
| task_set1   | n = 13, p = 4, x<sub>prec</sub> = 1/7  , x<sub>resid</sub> = 8/13  |
| task_set2   | n = 32, p = 8, x<sub>prec</sub> = 1/6  , x<sub>resid</sub> = 1     |
| task_set3   | n = 17, p = 4, x<sub>prec</sub> = 1/14 , x<sub>resid</sub> = 4/17  |
| task_set4   | n = 15, p = 5, x<sub>prec</sub> = 1/6  , x<sub>resid</sub> = 1/3   |

You must add a copyright notice when using case studies from our own collection in your work.
